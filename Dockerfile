FROM openjdk:16.0.2

RUN apt-get update && \
    apt-get install build-essential maven default-jdk cowsay netcat -y && \
    update-alternatives --config javac
COPY target/*.jar .

CMD ["mvn", "spring-boot:run"]
